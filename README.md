Arch/Artix Linux auto configuration scripts
--------------------------------------------

* 0-installation-files

Helper files to be used during the installation process.

* 1-system-wide-configuration

Scripts that make system-wide adjustments.

* 2-user-specific-configuration

Scripts that make user-specific adjustments.

* 3-extra-software-lists

Lists of the useful optional software.

